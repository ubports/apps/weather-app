# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the lomiri-weather-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-weather-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-12-23 07:55+0000\n"
"PO-Revision-Date: 2023-01-04 19:45+0000\n"
"Last-Translator: Anonymous <noreply@weblate.org>\n"
"Language-Team: Icelandic <https://hosted.weblate.org/projects/lomiri/lomiri-"
"weather-app/is/>\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n % 10 != 1 || n % 100 == 11;\n"
"X-Generator: Weblate 4.15.1-dev\n"

#: lomiri-weather-app.desktop.in:8
msgid "Weather"
msgstr ""

#: lomiri-weather-app.desktop.in:9
msgid "A weather forecast application for Ubuntu Touch"
msgstr ""

#: lomiri-weather-app.desktop.in:10
msgid ""
"weather;forecast;sunrise;sunset;moonphase;humidity;wind;rain;radar;"
"precipitation"
msgstr ""

#: src/app/qml/components/About.qml:27
msgid "Help"
msgstr ""

#: src/app/qml/components/About.qml:33
msgctxt "about page, section header, usage instructions"
msgid "Usage"
msgstr ""

#: src/app/qml/components/About.qml:33
msgctxt "about page, section header, troubleshooting"
msgid "Troubleshooting"
msgstr ""

#: src/app/qml/components/AboutGeneral.qml:26
msgid "About"
msgstr ""

#: src/app/qml/components/AboutGeneral.qml:78
msgid "Weather app"
msgstr ""

#: src/app/qml/components/AboutGeneral.qml:81
msgid "Version %1"
msgstr ""

#: src/app/qml/components/AboutGeneral.qml:96
msgid "Links"
msgstr ""

#: src/app/qml/components/AboutGeneral.qml:115
msgid "Get the sourcecode"
msgstr ""

#: src/app/qml/components/AboutGeneral.qml:116
msgid "Report issues"
msgstr ""

#: src/app/qml/components/AboutGeneral.qml:117
msgid "Help translate"
msgstr ""

#: src/app/qml/components/AboutGeneral.qml:141
msgid "Credits"
msgstr ""

#: src/app/qml/components/AboutGeneral.qml:161
msgid "Weather data by"
msgstr ""

#: src/app/qml/components/AboutGeneral.qml:167
msgid "Rainradar by"
msgstr ""

#: src/app/qml/components/AboutGeneral.qml:173
msgid "Coordinate lookup by"
msgstr ""

#: src/app/qml/components/AboutGeneral.qml:179
msgid "Location data lookup by"
msgstr ""

#: src/app/qml/components/AboutGeneral.qml:185
msgid "Sun and moon calculations by"
msgstr ""

#: src/app/qml/components/AboutGeneral.qml:191
msgid "Timezone converting by"
msgstr ""

#: src/app/qml/components/AboutTroubleshooting.qml:53
msgid ""
"If you encounter problems, please go through the following steps and try "
"again:"
msgstr ""

#: src/app/qml/components/AboutTroubleshooting.qml:68
msgctxt "troubleshooting, step title"
msgid "%1 Restart the app"
msgstr ""

#: src/app/qml/components/AboutTroubleshooting.qml:69
msgctxt "troubleshooting, description how to restart the app"
msgid "Close the app, then restart it as usual."
msgstr ""

#: src/app/qml/components/AboutTroubleshooting.qml:70
#: src/app/qml/components/AboutTroubleshooting.qml:75
msgid "Close weather app"
msgstr ""

#: src/app/qml/components/AboutTroubleshooting.qml:73
msgctxt "troubleshooting, step title"
msgid "%1 Update the app"
msgstr ""

#: src/app/qml/components/AboutTroubleshooting.qml:74
msgctxt "troubleshooting, description how to update the app"
msgid ""
"Close the app, then open OpenStore app and check for updates for weather app."
msgstr ""

#: src/app/qml/components/AboutTroubleshooting.qml:79
msgctxt "troubleshooting, step title"
msgid "%1 Reboot the device"
msgstr ""

#: src/app/qml/components/AboutTroubleshooting.qml:80
msgctxt "troubleshooting, description how to reboot"
msgid ""
"Press and hold the power button to raise the system dialog and select "
"'reboot' there. Alternatively use the same option from the system indicator."
msgstr ""

#: src/app/qml/components/AboutTroubleshooting.qml:81
msgid "Reboot device"
msgstr ""

#: src/app/qml/components/AboutTroubleshooting.qml:85
msgctxt "troubleshooting, step title"
msgid "%1 Clear the cache"
msgstr ""

#: src/app/qml/components/AboutTroubleshooting.qml:86
msgctxt "troubleshooting, description how to clear the cache"
msgid ""
"Clear the cache with the button below, use the app UT Tweak Tool or delete "
"the content of the <i>~/.cache/weather.ubports</i> folder."
msgstr ""

#: src/app/qml/components/AboutTroubleshooting.qml:87
msgid "Clear cache now"
msgstr ""

#: src/app/qml/components/AboutTroubleshooting.qml:91
msgctxt "troubleshooting, step title"
msgid "%1 Re-add locations"
msgstr ""

#: src/app/qml/components/AboutTroubleshooting.qml:92
msgctxt "troubleshooting, description how to delete the locations"
msgid ""
"Delete your locations in the locations list (see usage tab). Then add them "
"again. Or manually delete the database in <i>~/local/share/weather.ubports</"
"i>."
msgstr ""

#: src/app/qml/components/AboutTroubleshooting.qml:93
#: src/app/qml/components/AboutTroubleshooting.qml:99
msgid "Go back to main page"
msgstr ""

#: src/app/qml/components/AboutTroubleshooting.qml:97
msgctxt "troubleshooting, step title"
msgid "%1 Delete the apps settings"
msgstr ""

#: src/app/qml/components/AboutTroubleshooting.qml:98
msgctxt "troubleshooting, description how to delete the locations"
msgid ""
"Delete the apps settings either using the UT Tweak Tool or manually by "
"deleting the config file in <i>~/.config/weather.ubports</i>."
msgstr ""

#: src/app/qml/components/AboutTroubleshooting.qml:103
msgctxt "troubleshooting, step title"
msgid "%1 Check the log"
msgstr ""

#: src/app/qml/components/AboutTroubleshooting.qml:104
msgctxt "troubleshooting, description how to get the log and open an issue"
msgid ""
"If the instructions above did not solve the problem, please open an issue "
"and provide the app log. One way to obtain the log is with Logviewer app. "
"This app allows uploading the log to a pastebin. Provide the link to the "
"pasted log in the error report. Another option is to manually copy the log "
"file. It can be found under <i>~/.cache/upstart/application-click-com.ubuntu."
"weather_weather_VERSION.log</i>. If no logfile for weather app is present, "
"please temporary enable developer mode (system settings -> info -> developer "
"mode). This will make the log persist."
msgstr ""

#: src/app/qml/components/AboutTroubleshooting.qml:109
msgctxt "troubleshooting, step title"
msgid "%1 Open an issue on GitLab"
msgstr ""

#: src/app/qml/components/AboutTroubleshooting.qml:110
msgctxt "troubleshooting, description how to open an issue"
msgid ""
"Please go to <i>https://gitlab.com/ubports/apps/weather-app/-/issues</i> and "
"file an error report there. Please provide the following information: the "
"version of the app, the name of your device, your release channel, a "
"description of what you did and which problems you encountered."
msgstr ""

#: src/app/qml/components/AboutTroubleshooting.qml:111
msgid "Open issue tracker"
msgstr ""

#: src/app/qml/components/AboutTroubleshooting.qml:192
msgid "Cache cleared."
msgstr ""

#: src/app/qml/components/AboutTroubleshooting.qml:200
#: src/app/qml/components/NetworkErrorStateComponent.qml:64
#: src/app/qml/ui/AddLocationPage.qml:335
msgid "OK"
msgstr ""

#: src/app/qml/components/AboutUsage.qml:52
msgid "Access weather information"
msgstr ""

#: src/app/qml/components/AboutUsage.qml:53
msgctxt "usage, access weather information"
msgid ""
"You can swipe through locations or pick one by tapping on the indicator "
"dots. Tap on current information or a daily forecast for additional weather "
"details. Swipe the 3 hour forecast for more weather information."
msgstr ""

#: src/app/qml/components/AboutUsage.qml:56
msgid "Manage locations"
msgstr ""

#: src/app/qml/components/AboutUsage.qml:57
msgctxt "usage, manage locations"
msgid ""
"Add multiple locations via bottom edge or enable live GPS location in "
"settings. Manage locations via bottom edge locations page by swiping or "
"press & hold to remove and reorder."
msgstr ""

#: src/app/qml/components/AboutUsage.qml:60
msgid "Locations page list actions"
msgstr ""

#: src/app/qml/components/AboutUsage.qml:61
msgctxt "usage, manage locations"
msgid "To add new locations you need to have internet access!"
msgstr ""

#: src/app/qml/components/AboutUsage.qml:61
msgid "single tap a location to view its weather"
msgstr ""

#: src/app/qml/components/AboutUsage.qml:61
msgid "tap and hold on a location to"
msgstr ""

#: src/app/qml/components/AboutUsage.qml:61
msgid "a) enable sorting mode (drag the icon to reorder)"
msgstr ""

#: src/app/qml/components/AboutUsage.qml:61
msgid "b) multiselect locations to delete"
msgstr ""

#: src/app/qml/components/AboutUsage.qml:64
msgid "Rainradar"
msgstr ""

#: src/app/qml/components/AboutUsage.qml:65
msgctxt "usage, rainradar"
msgid ""
"You can view live rainradar by tapping the satellite icon in the header."
msgstr ""

#: src/app/qml/components/AboutUsage.qml:68
msgid "GPS live location"
msgstr ""

#: src/app/qml/components/AboutUsage.qml:69
msgctxt "usage, GPS live location"
msgid ""
"When GPS for current location is enabled, an additional 'live' location is "
"added to the list. This locations updates it's position on the move "
"according to GPS location."
msgstr ""

#: src/app/qml/components/AboutUsage.qml:72
msgid "Updating data"
msgstr ""

#: src/app/qml/components/AboutUsage.qml:73
msgctxt "usage, Updating data"
msgid ""
"Weather station data is updated on app start and at the interval specified "
"in the app settings. This does only work while the app is active."
msgstr ""

#: src/app/qml/components/DayDelegateExtraInfo.qml:48
msgid "Chance of precipitation"
msgstr ""

#: src/app/qml/components/DayDelegateExtraInfo.qml:58
msgid "Rain volume"
msgstr ""

#: src/app/qml/components/DayDelegateExtraInfo.qml:65
msgid "Snow volume"
msgstr ""

#: src/app/qml/components/DayDelegateExtraInfo.qml:72
msgid "Winds"
msgstr ""

#: src/app/qml/components/DayDelegateExtraInfo.qml:81
msgid "UV Index"
msgstr ""

#: src/app/qml/components/DayDelegateExtraInfo.qml:88
msgid "Sunshine duration"
msgstr ""

#: src/app/qml/components/DayDelegateExtraInfo.qml:94
msgid "Humidity"
msgstr ""

#: src/app/qml/components/DayDelegateExtraInfo.qml:101
msgid "Sunrise"
msgstr ""

#: src/app/qml/components/DayDelegateExtraInfo.qml:108
msgid "Sunset"
msgstr ""

#: src/app/qml/components/DayDelegateExtraInfo.qml:115
msgid "Pressure"
msgstr ""

#: src/app/qml/components/DayDelegateExtraInfo.qml:122
msgid "Moonphase"
msgstr ""

#: src/app/qml/components/HeadState/LocationsHeadState.qml:30
#: src/app/qml/components/HeadState/MultiSelectHeadState.qml:31
#: src/app/qml/ui/HomePage.qml:85
msgid "Locations"
msgstr ""

#: src/app/qml/components/HeadState/MultiSelectHeadState.qml:36
msgid "exit reorder mode"
msgstr ""

#: src/app/qml/components/HeadState/MultiSelectHeadState.qml:47
msgctxt "location list page, unselect all button description"
msgid "Unselect All"
msgstr ""

#: src/app/qml/components/HeadState/MultiSelectHeadState.qml:47
msgctxt "location list page, select all button description"
msgid "Select All"
msgstr ""

#: src/app/qml/components/HeadState/MultiSelectHeadState.qml:59
#, fuzzy
msgid "Delete"
msgstr "Delete"

#: src/app/qml/components/HomeHourly.qml:184
#, javascript-format
msgctxt ""
"%1 will be replaced with the value, unit % for chance of precipitation, "
"hourly forecast"
msgid "%1%"
msgstr ""

#. TRANSLATORS: rain/snow volume in inch, keep/remove the space before the unit according to your language specifications
#: src/app/qml/components/HomeHourly.qml:275
#: src/app/qml/components/HomeHourly.qml:291 src/app/qml/ui/HomePage.qml:262
#: src/app/qml/ui/HomePage.qml:291
msgid "%1 in"
msgstr ""

#. TRANSLATORS: rain volume in litre per squaremeter, keep/remove the space before the unit according to your language specifications
#: src/app/qml/components/HomeHourly.qml:278 src/app/qml/ui/HomePage.qml:265
msgid "%1 l/m²"
msgstr ""

#. TRANSLATORS: rain volume in millimeter, keep/remove the space before the unit according to your language specifications
#. TRANSLATORS: snow volume in millimeter, keep/remove the space before the unit according to your language specifications
#. TRANSLATORS: rain/snow volume in millimeter, keep/remove the space before the unit according to your language specifications
#: src/app/qml/components/HomeHourly.qml:281
#: src/app/qml/components/HomeHourly.qml:294 src/app/qml/ui/HomePage.qml:268
#: src/app/qml/ui/HomePage.qml:294
msgid "%1 mm"
msgstr ""

#. TRANSLATORS: snow volume in centimeter, keep/remove the space before the unit according to your language specifications
#. TRANSLATORS: snow volume in centimeter, keep/remove the space before the unit according to your language specifications
#. avoid 0.95 being rounded to 1.0 cm instead of 1 cm
#: src/app/qml/components/HomeHourly.qml:297 src/app/qml/ui/HomePage.qml:297
#: src/app/qml/ui/HomePage.qml:300 src/app/qml/ui/HomePage.qml:302
msgid "%1 cm"
msgstr ""

#: src/app/qml/components/HomePageEmptyStateComponent.qml:51
msgid "Searching for current location…"
msgstr ""

#: src/app/qml/components/HomePageEmptyStateComponent.qml:52
msgid "Cannot determine your location"
msgstr ""

#: src/app/qml/components/HomePageEmptyStateComponent.qml:62
msgid "Manually add a location by swiping up from the bottom of the display"
msgstr ""

#: src/app/qml/components/ListItemActions/Remove.qml:26
msgid "Remove"
msgstr ""

#: src/app/qml/components/LocationsListPageEmptyStateComponent.qml:36
msgid ""
"No network connection.\n"
"Unable to add new locations.\n"
"Please check your network settings and try again."
msgstr ""

#: src/app/qml/components/LocationsListPageEmptyStateComponent.qml:36
msgid "No locations found. Tap the plus icon to search for one."
msgstr ""

#: src/app/qml/components/NetworkErrorStateComponent.qml:49
msgid "Network Error"
msgstr ""

#: src/app/qml/components/NetworkErrorStateComponent.qml:58
msgid ""
"No network connection. Could not load weather data.\n"
"Please check your network settings and try again."
msgstr ""

#: src/app/qml/components/NoAPIKeyErrorStateComponent.qml:47
msgid "No API Keys Found"
msgstr ""

#: src/app/qml/components/NoAPIKeyErrorStateComponent.qml:56
msgid ""
"If you are a developer, please see the README file for instructions on how "
"to obtain your own Open Weather Map API key."
msgstr ""

#: src/app/qml/components/OnlineMap.qml:37
msgid "Rain radar"
msgstr ""

#: src/app/qml/data/moonphase.js:13
msgid "New moon"
msgstr ""

#: src/app/qml/data/moonphase.js:17
msgid "Waxing Crescent"
msgstr ""

#: src/app/qml/data/moonphase.js:21
msgid "First Quarter"
msgstr ""

#: src/app/qml/data/moonphase.js:25
msgid "Waxing Gibbous"
msgstr ""

#: src/app/qml/data/moonphase.js:29
msgid "Full moon"
msgstr ""

#: src/app/qml/data/moonphase.js:33
msgid "Waning Gibbous"
msgstr ""

#: src/app/qml/data/moonphase.js:37
msgid "Last Quarter"
msgstr ""

#: src/app/qml/data/moonphase.js:41
msgid "Waning Crescent"
msgstr ""

#: src/app/qml/data/moonphase.js:45
msgid "calculation error"
msgstr ""

#. TRANSLATORS: %1 does specify the parameters, %2 does specify the url for weather apps repo at gitlab
#: src/app/qml/lomiri-weather-app.qml:504
msgid ""
"Valid arguments for weather app are: %1 They will be managed by system. See "
"the README at %2 for a full comment about them"
msgstr ""

#: src/app/qml/ui/AddLocationPage.qml:41
msgid "Back"
msgstr ""

#: src/app/qml/ui/AddLocationPage.qml:83
msgid "Search city or select below"
msgstr ""

#: src/app/qml/ui/AddLocationPage.qml:109
msgctxt "location ID from geonames.org"
msgid "geonames.org ID: %1"
msgstr ""

#: src/app/qml/ui/AddLocationPage.qml:110
#: src/app/qml/ui/LocationsListPage.qml:284
msgctxt "Latitude and longitude (coordinates)"
msgid "lat: %1, lon: %2"
msgstr ""

#: src/app/qml/ui/AddLocationPage.qml:197
msgid ""
"Could not load data. Please open an issue and provide the following error "
"message:"
msgstr ""

#: src/app/qml/ui/AddLocationPage.qml:199
msgid ""
"Could not load data. Provider server unavailable. Please ensure you are "
"connected to the Internet."
msgstr ""

#: src/app/qml/ui/AddLocationPage.qml:310
msgid "No city found"
msgstr ""

#: src/app/qml/ui/AddLocationPage.qml:332
msgid "Location already added."
msgstr ""

#. TRANSLATORS: N = North, wind bearing, abbreviated
#: src/app/qml/ui/HomePage.qml:132
msgid "N"
msgstr ""

#. TRANSLATORS: NE = North East, wind bearing, abbreviated
#: src/app/qml/ui/HomePage.qml:135
msgid "NE"
msgstr ""

#. TRANSLATORS: E = East, wind bearing, abbreviated
#: src/app/qml/ui/HomePage.qml:138
msgid "E"
msgstr ""

#. TRANSLATORS: SE = South East, wind bearing, abbreviated
#: src/app/qml/ui/HomePage.qml:141
msgid "SE"
msgstr ""

#. TRANSLATORS: S = South, wind bearing, abbreviated
#: src/app/qml/ui/HomePage.qml:144
msgid "S"
msgstr ""

#. TRANSLATORS: SW = South West, wind bearing, abbreviated
#: src/app/qml/ui/HomePage.qml:147
msgid "SW"
msgstr ""

#. TRANSLATORS: W = West, wind bearing, abbreviated
#: src/app/qml/ui/HomePage.qml:150
msgid "W"
msgstr ""

#. TRANSLATORS: NW = NorthWest, wind bearing, abbreviated
#: src/app/qml/ui/HomePage.qml:153
msgid "NW"
msgstr ""

#. TRANSLATORS: temperatures in °F, keep/remove the space before the unit according to your language specifications
#: src/app/qml/ui/HomePage.qml:210
msgid "%1 °F"
msgstr ""

#. TRANSLATORS: temperatures in °C, keep/remove the space before the unit according to your language specifications
#: src/app/qml/ui/HomePage.qml:216
msgid "%1 °C"
msgstr ""

#. TRANSLATORS: wind speed in m/s, keep/remove the space before the unit according to your language specifications
#: src/app/qml/ui/HomePage.qml:229
msgid "%1 m/s"
msgstr ""

#. TRANSLATORS: wind speed in km/h, keep/remove the space before the unit according to your language specifications
#: src/app/qml/ui/HomePage.qml:233
msgid "%1 km/h"
msgstr ""

#. TRANSLATORS: wind speed in mph, keep/remove the space before the unit according to your language specifications
#: src/app/qml/ui/HomePage.qml:237
msgid "%1 mph"
msgstr ""

#: src/app/qml/ui/HomePage.qml:315
msgid "low"
msgstr ""

#: src/app/qml/ui/HomePage.qml:317
msgid "moderate"
msgstr ""

#: src/app/qml/ui/HomePage.qml:319
msgid "high"
msgstr ""

#: src/app/qml/ui/HomePage.qml:321
msgid "very high"
msgstr ""

#: src/app/qml/ui/HomePage.qml:323
msgid "extreme"
msgstr ""

#: src/app/qml/ui/HomePage.qml:325
msgid "no valid value"
msgstr ""

#: src/app/qml/ui/LocationPane.qml:73 src/app/qml/ui/LocationPane.qml:127
msgid "Today"
msgstr ""

#: src/app/qml/ui/LocationPane.qml:131
msgid "updated %1 day ago"
msgid_plural "updated %1 days ago"
msgstr[0] ""
msgstr[1] ""

#: src/app/qml/ui/LocationPane.qml:133
msgid "updated %1 hour ago"
msgid_plural "updated %1 hours ago"
msgstr[0] ""
msgstr[1] ""

#: src/app/qml/ui/LocationPane.qml:135
msgid "updated %1 minute ago"
msgid_plural "updated %1 minutes ago"
msgstr[0] ""
msgstr[1] ""

#: src/app/qml/ui/LocationPane.qml:137
msgid "updated recently"
msgstr ""

#. TRANSLATORS: % (percent) is the unit for humidity or chance or precipitation, add a space before the unit according to your language specifications
#: src/app/qml/ui/LocationPane.qml:323
#, javascript-format
msgctxt ""
"%1 will be replaced with the value, unit % for humidity, daily forecast"
msgid "%1%"
msgstr ""

#. TRANSLATORS: hectopascal, unit for air pressure, only use abbreviated
#: src/app/qml/ui/LocationPane.qml:330
msgctxt "pressure, daily forecast"
msgid "%1 hPa"
msgstr ""

#. TRANSLATORS: first value is wind speed, second value is wind bearing, reorder and keep/remove space as suitable
#: src/app/qml/ui/LocationPane.qml:335
msgctxt "wind data, daily forecast"
msgid "%1 %2"
msgstr ""

#. TRANSLATORS: % (percent) is the unit for humidity or chance or precipitation, add a space before the unit according to your language specifications
#: src/app/qml/ui/LocationPane.qml:338
#, javascript-format
msgctxt ""
"%1 will be replaced with the value, unit % for chance of precipitation, "
"daily forecast"
msgid "%1%"
msgstr ""

#. TRANSLATORS: feels like refers to the current temperature as how it feels adjusted e.g. by wind
#: src/app/qml/ui/LocationPane.qml:366
msgctxt "current weather"
msgid "feels like %1"
msgstr ""

#. TRANSLATORS: %1 is cloud coverage in percent, keep/remove the space before the unit according to your language specifications
#: src/app/qml/ui/LocationPane.qml:368
msgctxt "current weather"
msgid "Clouds: %1 %"
msgstr ""

#. TRANSLATORS: %1 is wind speed including unit and %2 is wind direction
#: src/app/qml/ui/LocationPane.qml:370
msgctxt "current weather"
msgid "Wind: %1 %2"
msgstr ""

#: src/app/qml/ui/LocationPane.qml:371
msgctxt "current weather"
msgid "Wind gusts: %1"
msgstr ""

#. TRANSLATORS: humidity in percent, keep/remove the space before the unit according to your language specifications
#: src/app/qml/ui/LocationPane.qml:373
msgctxt "humidity, current weather"
msgid "Humidity: %1 %"
msgstr ""

#: src/app/qml/ui/LocationPane.qml:374
msgctxt "current weather"
msgid "UV Index: %1"
msgstr ""

#: src/app/qml/ui/LocationPane.qml:375
msgctxt "current weather chance of precipitation"
msgid "%1 %"
msgstr ""

#: src/app/qml/ui/LocationPane.qml:376
msgctxt "current weather"
msgid "Visibility: %1"
msgstr ""

#: src/app/qml/ui/LocationsListPage.qml:102
msgid "Current Location"
msgstr ""

#: src/app/qml/ui/LocationsListPage.qml:283
msgctxt "Location ID from geonames.org"
msgid "geonames.org ID: %1"
msgstr ""

#: src/app/qml/ui/SettingsPage.qml:28
msgid "Settings"
msgstr ""

#: src/app/qml/ui/SettingsPage.qml:97
msgid "Data is only updated while the app is open and focused"
msgstr ""

#. TRANSLATORS: name of the weather data service provider, only translate if needed
#: src/app/qml/ui/settings/DataProviderPage.qml:31
msgid "Open-Meteo"
msgstr ""

#: src/app/qml/ui/settings/DataProviderPage.qml:37
msgid "Data Provider"
msgstr ""

#: src/app/qml/ui/settings/LocationPage.qml:32
msgid "Detect current location"
msgstr ""

#. TRANSLATORS: millimeter, metric unit for rain/snow given as millimeter per hour, only use abbreviated
#: src/app/qml/ui/settings/RainUnitPage.qml:31
msgid "mm"
msgstr ""

#. TRANSLATORS: inch, imperial unit for rain/snow given as inch per hour, only use abbreviated
#: src/app/qml/ui/settings/RainUnitPage.qml:34
msgid "in"
msgstr ""

#. TRANSLATORS: liter, metric unit for rain/snow given as liter per squaremeter per hour, only use abbreviated
#: src/app/qml/ui/settings/RainUnitPage.qml:37
msgid "l/m²"
msgstr ""

#: src/app/qml/ui/settings/RainUnitPage.qml:43
msgid "Rain volume unit"
msgstr ""

#: src/app/qml/ui/settings/RefreshIntervalPage.qml:30
#: src/app/qml/ui/settings/RefreshIntervalPage.qml:31
#: src/app/qml/ui/settings/RefreshIntervalPage.qml:32
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] ""
msgstr[1] ""

#: src/app/qml/ui/settings/RefreshIntervalPage.qml:33
#: src/app/qml/ui/settings/RefreshIntervalPage.qml:34
#: src/app/qml/ui/settings/RefreshIntervalPage.qml:35
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] ""
msgstr[1] ""

#: src/app/qml/ui/settings/RefreshIntervalPage.qml:41
msgid "Refresh Interval"
msgstr ""

#. TRANSLATORS: degree celsius, metric unit for temperature, only use abbreviated
#: src/app/qml/ui/settings/TempUnitPage.qml:31
msgid "°C"
msgstr ""

#. TRANSLATORS: degree fahrenheit, imperial unit for temperature, only use abbreviated
#: src/app/qml/ui/settings/TempUnitPage.qml:34
msgid "°F"
msgstr ""

#: src/app/qml/ui/settings/TempUnitPage.qml:40
msgid "Temperature unit"
msgstr ""

#: src/app/qml/ui/settings/TemperatureColorPage.qml:30
msgid "no colors"
msgstr ""

#: src/app/qml/ui/settings/TemperatureColorPage.qml:31
msgid "blue/yellow"
msgstr ""

#: src/app/qml/ui/settings/TemperatureColorPage.qml:32
msgid "blue/orange"
msgstr ""

#: src/app/qml/ui/settings/TemperatureColorPage.qml:33
msgid "blue/red"
msgstr ""

#: src/app/qml/ui/settings/TemperatureColorPage.qml:39
msgid "Temperature colors"
msgstr ""

#: src/app/qml/ui/settings/ThemePage.qml:30
msgid "System theme"
msgstr ""

#: src/app/qml/ui/settings/ThemePage.qml:31
msgid "SuruDark theme"
msgstr ""

#: src/app/qml/ui/settings/ThemePage.qml:32
msgid "Ambiance theme"
msgstr ""

#: src/app/qml/ui/settings/ThemePage.qml:38
msgid "Style"
msgstr ""

#: src/app/qml/ui/settings/TodayColorPage.qml:33
msgid "Grey background for current data?"
msgstr ""

#. TRANSLATORS: meter per second, metric SI unit for wind speed, only use abbreviated
#: src/app/qml/ui/settings/WindUnitPage.qml:32
msgid "m/s"
msgstr ""

#. TRANSLATORS: kilometer per hour, metric unit for wind speed, only use abbreviated
#: src/app/qml/ui/settings/WindUnitPage.qml:35
msgid "km/h"
msgstr ""

#. TRANSLATORS: miles per hour, imperial unit for wind speed, only use abbreviated
#: src/app/qml/ui/settings/WindUnitPage.qml:38
msgid "mph"
msgstr ""

#: src/app/qml/ui/settings/WindUnitPage.qml:44
msgid "Wind speed unit"
msgstr ""

#: src/plugin/data_point.cpp:34
msgid "clear"
msgstr ""

#: src/plugin/data_point.cpp:36
msgid "dissolving clouds"
msgstr ""

#: src/plugin/data_point.cpp:38
msgid "partly cloudly"
msgstr ""

#: src/plugin/data_point.cpp:40
msgid "overcast"
msgstr ""

#: src/plugin/data_point.cpp:42
msgid "smoke or ash"
msgstr ""

#: src/plugin/data_point.cpp:44
msgid "haze"
msgstr ""

#: src/plugin/data_point.cpp:46
msgid "light dust"
msgstr ""

#: src/plugin/data_point.cpp:48
msgid "dust"
msgstr ""

#: src/plugin/data_point.cpp:50
msgid "heavy dust"
msgstr ""

#: src/plugin/data_point.cpp:52
msgid "nearby dust storm"
msgstr ""

#: src/plugin/data_point.cpp:54
msgid "mist"
msgstr ""

#: src/plugin/data_point.cpp:56
msgid "light fog"
msgstr ""

#: src/plugin/data_point.cpp:58
msgid "continuous fog"
msgstr ""

#: src/plugin/data_point.cpp:60
msgid "visible lightning"
msgstr ""

#: src/plugin/data_point.cpp:62
msgid "precipitation"
msgstr ""

#: src/plugin/data_point.cpp:64
msgid "distant precipitation"
msgstr ""

#: src/plugin/data_point.cpp:66
msgid "nearby precipitation"
msgstr ""

#: src/plugin/data_point.cpp:68
msgid "dry thunderstorm"
msgstr ""

#: src/plugin/data_point.cpp:70
msgid "squalls"
msgstr ""

#: src/plugin/data_point.cpp:72
msgid "funnel clouds"
msgstr ""

#: src/plugin/data_point.cpp:74
msgid "drizzle"
msgstr ""

#: src/plugin/data_point.cpp:76
msgid "rain"
msgstr ""

#: src/plugin/data_point.cpp:78
msgid "snow"
msgstr ""

#: src/plugin/data_point.cpp:80
msgid "rain and snow"
msgstr ""

#: src/plugin/data_point.cpp:82
msgid "freezing rain"
msgstr ""

#: src/plugin/data_point.cpp:84
msgid "rain shower"
msgstr ""

#: src/plugin/data_point.cpp:86
msgid "snow shower"
msgstr ""

#: src/plugin/data_point.cpp:88
msgid "hail shower"
msgstr ""

#: src/plugin/data_point.cpp:90 src/plugin/data_point.cpp:122
msgid "fog"
msgstr ""

#: src/plugin/data_point.cpp:92
msgid "thunderstorm"
msgstr ""

#: src/plugin/data_point.cpp:94
msgid "decreasing dust storm"
msgstr ""

#: src/plugin/data_point.cpp:96
msgid "dust storm"
msgstr ""

#: src/plugin/data_point.cpp:98
msgid "increasing dust storm"
msgstr ""

#: src/plugin/data_point.cpp:100
msgid "decreasing severe dust storm"
msgstr ""

#: src/plugin/data_point.cpp:102
msgid "severe dust storm"
msgstr ""

#: src/plugin/data_point.cpp:104
msgid "increasing severe dust storm"
msgstr ""

#: src/plugin/data_point.cpp:106
msgid "low blowing snow"
msgstr ""

#: src/plugin/data_point.cpp:108
msgid "low heavy drifting snow"
msgstr ""

#: src/plugin/data_point.cpp:110
msgid "high blowing snow"
msgstr ""

#: src/plugin/data_point.cpp:112
msgid "high heavy drifting snow"
msgstr ""

#: src/plugin/data_point.cpp:114
msgid "distant fog"
msgstr ""

#: src/plugin/data_point.cpp:116
msgid "patchy fog"
msgstr ""

#: src/plugin/data_point.cpp:119
msgid "decreasing fog"
msgstr ""

#: src/plugin/data_point.cpp:125
msgid "increasing fog"
msgstr ""

#: src/plugin/data_point.cpp:128
msgid "fog with rime"
msgstr ""

#: src/plugin/data_point.cpp:130
msgid "intermittent slight drizzle"
msgstr ""

#: src/plugin/data_point.cpp:132
msgid "continuous slight drizzle"
msgstr ""

#: src/plugin/data_point.cpp:134
msgid "intermittent drizzle"
msgstr ""

#: src/plugin/data_point.cpp:136
msgid "continuous drizzle"
msgstr ""

#: src/plugin/data_point.cpp:138
msgid "intermittent heavy drizzle"
msgstr ""

#: src/plugin/data_point.cpp:140
msgid "continuous heavy drizzle"
msgstr ""

#: src/plugin/data_point.cpp:142
msgid "freezing slight drizzle"
msgstr ""

#: src/plugin/data_point.cpp:144
msgid "freezing heavy drizzle"
msgstr ""

#: src/plugin/data_point.cpp:146
msgid "slight rain drizzle"
msgstr ""

#: src/plugin/data_point.cpp:148
msgid "heavy rain drizzle"
msgstr ""

#: src/plugin/data_point.cpp:150
msgid "intermittent slight rain"
msgstr ""

#: src/plugin/data_point.cpp:152
msgid "continuous slight rain"
msgstr ""

#: src/plugin/data_point.cpp:154
msgid "intermittent rain"
msgstr ""

#: src/plugin/data_point.cpp:156
msgid "continuous rain"
msgstr ""

#: src/plugin/data_point.cpp:158
msgid "intermittent heavy rain"
msgstr ""

#: src/plugin/data_point.cpp:160
msgid "continuous heavy rain"
msgstr ""

#: src/plugin/data_point.cpp:162
msgid "freezing slight rain"
msgstr ""

#: src/plugin/data_point.cpp:164
msgid "freezing heavy rain"
msgstr ""

#: src/plugin/data_point.cpp:166
msgid "slight rain drizzle with snow"
msgstr ""

#: src/plugin/data_point.cpp:168
msgid "heavy rain drizzle with snow"
msgstr ""

#: src/plugin/data_point.cpp:170
msgid "intermittent slight snowflakes"
msgstr ""

#: src/plugin/data_point.cpp:172
msgid "continuous slight snowflakes"
msgstr ""

#: src/plugin/data_point.cpp:174
msgid "intermittent snowflakes"
msgstr ""

#: src/plugin/data_point.cpp:176
msgid "continuous snowflakes"
msgstr ""

#: src/plugin/data_point.cpp:178
msgid "intermittent heavy snowflakes"
msgstr ""

#: src/plugin/data_point.cpp:180
msgid "continuous heavy snowflakes"
msgstr ""

#: src/plugin/data_point.cpp:182
msgid "diamond dust"
msgstr ""

#: src/plugin/data_point.cpp:184
msgid "snow grains"
msgstr ""

#: src/plugin/data_point.cpp:186
msgid "snow stars"
msgstr ""

#: src/plugin/data_point.cpp:188
msgid "ice pellets"
msgstr ""

#: src/plugin/data_point.cpp:190
msgid "slight rain shower"
msgstr ""

#: src/plugin/data_point.cpp:192
msgid "heavy rain shower"
msgstr ""

#: src/plugin/data_point.cpp:194
msgid "violent rain shower"
msgstr ""

#: src/plugin/data_point.cpp:196
msgid "slight rain shower with snow"
msgstr ""

#: src/plugin/data_point.cpp:198
msgid "heavy rain shower with snow"
msgstr ""

#: src/plugin/data_point.cpp:200
msgid "slight snow shower"
msgstr ""

#: src/plugin/data_point.cpp:202
msgid "heavy snow shower"
msgstr ""

#: src/plugin/data_point.cpp:204
msgid "slight small hail"
msgstr ""

#: src/plugin/data_point.cpp:206
msgid "heavy small hail"
msgstr ""

#: src/plugin/data_point.cpp:208
msgid "slight hail without thunder"
msgstr ""

#: src/plugin/data_point.cpp:210
msgid "heavy hail without thunder"
msgstr ""

#: src/plugin/data_point.cpp:212
msgid "slight rain after thunderstorm"
msgstr ""

#: src/plugin/data_point.cpp:214
msgid "heavy rain after thunderstorm"
msgstr ""

#: src/plugin/data_point.cpp:216
msgid "slight snow or hail after thunderstorm"
msgstr ""

#: src/plugin/data_point.cpp:218
msgid "heavy snow or hail after thunderstorm"
msgstr ""

#: src/plugin/data_point.cpp:220
msgid "slight thunderstorm without hail"
msgstr ""

#: src/plugin/data_point.cpp:222
msgid "slight thunderstorm with hail"
msgstr ""

#: src/plugin/data_point.cpp:224
msgid "heavy thunderstorm without hail"
msgstr ""

#: src/plugin/data_point.cpp:226
msgid "thunderstorm with dust storm"
msgstr ""

#: src/plugin/data_point.cpp:228
msgid "heavy thunderstorm with hail"
msgstr ""
