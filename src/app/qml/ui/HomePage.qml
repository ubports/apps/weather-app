/*
 * Copyright (C) 2015-2016 Canonical Ltd.
 * Copyright (C) 2016 Nekhelesh Ramananthan
 * Copyright (C) 2016-2017 Andrew Hayzen
 * Copyright (C) 2019 Joan CiberSheep
 * Copyright (C) 2019 Krille-chan
 * Copyright (C) 2019-2022 Daniel Frost
 * Copyright (C) 2022 Guido Berhoerster
 * Copyright (C) 2024 Mike Gabriel
 * Copyright (C) 2024 Maciej Sopyło <me@klh.io>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This file is part of Lomiri Weather App
 *
 * Lomiri Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Lomiri Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import "../components"

Page {
    // Set to null otherwise the header is shown (but blank) over the top of the listview
    id: homePage
    objectName: "homePage"
    flickable: null
    width: weatherApp.width

    Rectangle {
      //when swiping BottomEdge the DayDelegate underneath gets clicked
      //transparent MouseArea to catch click events near BottomEdge
      id: avoidDayDelegateClick
      width: parent.width
      height: units.gu(4)
      color: "transparent"
      z:1 //raise above DayDelegate's
      anchors {
          bottom: parent.bottom
          left: parent.left
          right: parent.right
      }
      MouseArea {
          anchors.fill: parent
          cursorShape: Qt.PointingHandCursor
          propagateComposedEvents: false
          hoverEnabled: false
          onClicked: { bottomEdge.commit()}
      }
    }

    BottomEdge {
        id: bottomEdge
        height: parent.height
        preloadContent: false //needs to be set to false to allow theme changing without errors

        // Note: cannot use contentUrl and preload until pad.lv/1604509
        contentComponent: locationsListPage

        Component {
            id: locationsListPage
            LocationsListPage {
                height: bottomEdge.height
                width: bottomEdge.width

                onPop: bottomEdge.collapse()
                onThemeChanged: {
                        //HACK: Dirty hack to avoid bottomEdge stops working after changing themes
                        //preloadContent needs to be set to false for this to work
                        bottomEdge.commit()
                        bottomEdge.collapse()
                    }
            }
        }
        hint.text: bottomEdge.hint.status == BottomEdgeHint.Locked ? i18n.tr("Locations") : ""
    }

    //system weather icons are used for forecast icons (3h and daily)
    //rain and snow icons are later replaced depending on downfall volume
    property var iconMap: {
        "sun": "weather-clear-symbolic",
        "moon": "weather-clear-night-symbolic",
        "few_cloud_sun": "weather-few-clouds-symbolic",
        "few_cloud_moon": "weather-few-clouds-night-symbolic",
        "scattered_cloud": "weather-clouds-symbolic",
        "cloud": "weather-overcast-symbolic",
        "showers": "weather-showers-scattered-symbolic",
        "rain": "weather-showers-symbolic", //no rain icon available (??) -> maybe rename icon upstream?
        "thunder": "weather-storm-symbolic",
        "snow": "weather-snow-symbolic",
        "fog": "weather-fog-symbolic"
    }
//other available weather icons currently unused
// weather-flurries-symbolic
// weather-sleet-symbolic
// weather-hazy-symbolic
// weather-severe-alert-symbolic

    //local stored svg's are used for todays condition icon
    //"normal" icons lines are too thick when scaled up that big
    property var imageMap: {
        "sun": Qt.resolvedUrl("qrc:/weather-clear.svg"),
        "moon": Qt.resolvedUrl("qrc:/weather-clear-night.svg"),
        "few_cloud_sun": Qt.resolvedUrl("qrc:/weather-few-clouds.svg"),
        "few_cloud_moon": Qt.resolvedUrl("qrc:/weather-few-clouds-night.svg"),
        "scattered_cloud": Qt.resolvedUrl("qrc:/weather-clouds.svg"),
        "cloud": Qt.resolvedUrl("qrc:/weather-overcast.svg"),
        "showers": Qt.resolvedUrl("qrc:/weather-showers-scattered.svg"),
        "rain": Qt.resolvedUrl("qrc:/weather-showers.svg"),
        "thunder": Qt.resolvedUrl("qrc:/weather-storm.svg"),
        "snow": Qt.resolvedUrl("qrc:/weather-snow.svg"),
        "fog": Qt.resolvedUrl("qrc:/weather-fog.svg"),
    }

    /*
     wrapper function to extract wind bearings delivered from API and return them as translatable strings
    */
    function getWindBearing(windBearing) {

        if (windBearing == "N") {
            // TRANSLATORS: N = North, wind bearing, abbreviated
            return i18n.tr("N");
        } else if (windBearing == "NE") {
            // TRANSLATORS: NE = North East, wind bearing, abbreviated
            return i18n.tr("NE");
        } else if (windBearing == "E") {
            // TRANSLATORS: E = East, wind bearing, abbreviated
            return i18n.tr("E");
        } else if (windBearing == "SE") {
            // TRANSLATORS: SE = South East, wind bearing, abbreviated
            return i18n.tr("SE");
        } else if (windBearing == "S") {
            // TRANSLATORS: S = South, wind bearing, abbreviated
            return i18n.tr("S");
        } else if (windBearing == "SW") {
            // TRANSLATORS: SW = South West, wind bearing, abbreviated
            return i18n.tr("SW");
        } else if (windBearing == "W") {
            // TRANSLATORS: W = West, wind bearing, abbreviated
            return i18n.tr("W");
        } else if (windBearing == "NW") {
            // TRANSLATORS: NW = NorthWest, wind bearing, abbreviated
            return i18n.tr("NW");
        } else {
            //if wind bearing is none of the above just push it trough without translation
            return windBearing;
        }
    }

    /*
      Format date object by given format.
    */
    function formatTimestamp(dateData, format) {
        return Qt.formatDate(getDate(dateData), i18n.tr(format))
    }

    /*
      Return date short format, which is default, so no special formatting needed.
    */
    function formatTimestampLocaleShort(dateData) {
        return Qt.formatDate(getDate(dateData))
    }

    /*
      Format time object by given format.
    */
    function formatTime(dateData, format) {
        return Qt.formatTime(getDate(dateData), i18n.tr(format))
    }

    /*
      Get Date object from dateData including hour data.
    */
    function getDate(dateData) {
        return new Date(dateData.year, dateData.month, dateData.date, dateData.hours, dateData.minutes)
    }

    /*
      Get Date object from dateData with additional offset [in minutes].
    */
    function getCustomDate(dateData,offsetMin) {
        return new Date(dateData.year, dateData.month, dateData.date, dateData.hours, dateData.minutes + offsetMin)
    }

    function emptyIfUndefined(variable, append) {
        if (append === undefined) {
            append = ""
        }
        if (variable === undefined || variable === -255) {
            variable = ""
            append = "" //without valid value, drop append as well
        }
        return variable + append
    }

    //add temperature unit and convert if necessary
    function getTemp(tempToConvert, showunit) {
        var value = tempToConvert
        //query settings here, so function can be called without unit, used for current temperature
        if (settings.tempScale === "°F") {
            value = Math.round(calcFahrenheit(value))
            if (showunit) {
                //TRANSLATORS: temperatures in °F, keep/remove the space before the unit according to your language specifications
                value = i18n.tr("%1 °F").arg(value)
            }
        } else {
            value = Math.round(value)
            if (showunit) {
                //TRANSLATORS: temperatures in °C, keep/remove the space before the unit according to your language specifications
                value = i18n.tr("%1 °C").arg(value)
            }
        }
        return value;
    }

    //add wind speed unit and convert if necessary
    function getWindSpeed(speed) {
        var value = speed //raw wind speed in m/s
        var speedstring  //to hold the string, so translated units with/without space after the number can be used
        if (settings.windUnit === "m/s") {
            value = Math.round(value*10)/10 // keep one decimal
            //TRANSLATORS: wind speed in m/s, keep/remove the space before the unit according to your language specifications
            speedstring = i18n.tr("%1 m/s").arg(value.toString().replace(".",Qt.locale().decimalPoint));
        } else if (settings.windUnit === "km/h") {
            value = Math.round(calcKmh(value))
            //TRANSLATORS: wind speed in km/h, keep/remove the space before the unit according to your language specifications
            speedstring = i18n.tr("%1 km/h").arg(value);
        } else if (settings.windUnit === "mph") {
            value = Math.round(calcMph(value))
            //TRANSLATORS: wind speed in mph, keep/remove the space before the unit according to your language specifications
            speedstring = i18n.tr("%1 mph").arg(value);
        } else {
            console.log("wind unit error ")
        }
        return speedstring;
    }

    //add rain volume unit, adjust decimals and convert if necessary
    //called from LocationPage for current info (currently na) and DayDelegateExtraInfo/daily info, HomeHourly for hourly info
    function getRain(rain) {
        var value = rain
        var label = ""
        if (settings.precipUnit == "in") {
            // convert to inch if needed and round numbers
            // keep two dezimals
            value = Math.round(calcInch(value)*100)/100;
        } else {
            // keep one dezimal
            // l/sqm is the same number as mm, no further adjustment needed
            value =  Math.round(value*10)/10
        }
        // add unit according to setting
        // put in decimal separator according to set locale
        if (settings.precipUnit == "in") {
            //TRANSLATORS: rain/snow volume in inch, keep/remove the space before the unit according to your language specifications
            label = i18n.tr("%1 in").arg(localeDezimalSeparator(value.toFixed(2)))
        } else if (settings.precipUnit == "l") {
            //TRANSLATORS: rain volume in litre per squaremeter, keep/remove the space before the unit according to your language specifications
            label = i18n.tr("%1 l/m²").arg(localeDezimalSeparator(value))
        } else {
            //TRANSLATORS: rain/snow volume in millimeter, keep/remove the space before the unit according to your language specifications
            label = i18n.tr("%1 mm").arg(localeDezimalSeparator(value))
        }
        return label;
    }

    //add snow volume unit, adjust decimals and convert if necessary
    //called from LocationPage for current info (currently na) and DayDelegateExtraInfo/daily info, HomeHourly for hourly info
    function getSnow(snow) {
        var value = snow
        var label = ""
        if (settings.snowUnit == "in") {
            // convert to inch if needed and round numbers
            // keep two dezimals
            value = Math.round(calcInch(value)*100)/100;
        } else {
            // keep one dezimal
            // l/sqm is the same number as mm, no further adjustment needed
            value =  Math.round(value*10)/10
        }
        // add unit according to setting
        // put in decimal separator according to set locale
        if (settings.snowUnit == "in") {
            //TRANSLATORS: rain/snow volume in inch, keep/remove the space before the unit according to your language specifications
            label = i18n.tr("%1 in").arg(localeDezimalSeparator(value.toFixed(2)))
        } else if (settings.snowUnit == "mm") {
            //TRANSLATORS: rain/snow volume in millimeter, keep/remove the space before the unit according to your language specifications
            label = i18n.tr("%1 mm").arg(localeDezimalSeparator(value))
        } else {
            //TRANSLATORS: snow volume in centimeter, keep/remove the space before the unit according to your language specifications
            label = i18n.tr("%1 cm").arg(localeDezimalSeparator(value))
            //TRANSLATORS: snow volume in centimeter, keep/remove the space before the unit according to your language specifications
            if (value < 0.95) { //avoid 0.95 being rounded to 1.0 cm instead of 1 cm
                label = i18n.tr("%1 cm").arg(localeDezimalSeparator(value.toFixed(1)))
            } else {
                label = i18n.tr("%1 cm").arg(localeDezimalSeparator(value.toFixed(0)))
            }
        }
        return label;
    }

    /*
      add UV-index level based on WHO classification
      https://www.who.int/news-room/questions-and-answers/item/radiation-the-ultraviolet-(uv)-index
    */
    function getUVLevel(value) {
        var level
        if (value >= 0 && value < 3) {
            level = i18n.tr("low")
        } else if (value >= 3 && value < 6) {
            level = i18n.tr("moderate")
        } else if (value >= 6 && value < 8) {
            level = i18n.tr("high")
        } else if (value >= 8 && value < 11) {
            level = i18n.tr("very high")
        } else if (value >= 11) {
            level = i18n.tr("extreme")
        } else {
            level = i18n.tr("no valid value")
        }

        return " (%1)".arg(level)
    }

    /*
      provide visibility distance based on visibility range in different units
    */
    function getVisibility(value) {
        var visibility

        if (value > 1000) {
            visibility = localeDezimalSeparator((value / 1000).toFixed(1)) + " km"
        } else {
            visibility = localeDezimalSeparator(value) + " m"
        }

        return " %1".arg(visibility)
    }

    /*
    unit converting helper functions
    */
    //convert mm to in, both volume per hour
    function calcInch(mm) {
        return mm/25.4;
    }
    //convert celsius to fahrenheit
    function calcFahrenheit(celsius) {
            return celsius * 1.8 + 32;
    }
    // convert m/s to mph
    function calcMph(ms) {
        return ms*2.24;
    }

    //convert
    function calcKmh(ms) {
        return ms*3.6;
    }
    //
    function convertKmhToMph(kmh) {
        return kmh*0.621;
    }


    // Do not show the Page Header
    header: PageHeader {
        visible: false
    }

    /*
      ListView for locations with snap-scrolling horizontally.
    */
    ListView {
        id: locationPages
        objectName: "locationPages"
        anchors {
            fill: parent
        }
        contentHeight: parent.height - bottomEdge.hint.height
        currentIndex: settings.current
        delegate: LocationPane {
            objectName: "locationPane" + index
        }
        highlightRangeMode: ListView.StrictlyEnforceRange
        highlightMoveDuration: 50
        highlightMoveVelocity: 600
        model: weatherApp.locationsList.length
        orientation: ListView.Horizontal
        // TODO with snapMode, currentIndex is not properly set and setting currentIndex fails
        //snapMode: ListView.SnapOneItem

        property bool loaded: false

        signal collapseOtherDelegates(int index)

        onCurrentIndexChanged: {
            if (loaded) {
                // FIXME: when a model is reloaded this causes the currentIndex to be lost
                settings.current = currentIndex

                collapseOtherDelegates(-1)  // collapse all
            }
        }
        onModelChanged: {
            currentIndex = settings.current

            if (model > 0) {
                loading = false
                loaded = true
            }
        }
        onVisibleChanged: {
            if (!visible && loaded) {
                collapseOtherDelegates(-1)  // collapse all
            }
        }

        // TODO: workaround for not being able to use snapMode property
        Component.onCompleted: {
            var scaleFactor = units.gridUnit * 10;
            maximumFlickVelocity = maximumFlickVelocity * scaleFactor;
            flickDeceleration = flickDeceleration * scaleFactor;
        }

        Connections {
            target: settings
            onCurrentChanged: {
                locationPages.currentIndex = settings.current
            }
        }
    }

    LoadingIndicator {
        id: loadingIndicator
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }
        processing: loading
    }

    Loader {
        active: (locationsList === null || locationsList.length === 0) && mainPageStack.depth === 1
        anchors {
            fill: parent
        }
        asynchronous: true
        source: "../components/HomePageEmptyStateComponent.qml"
        visible: status === Loader.Ready && active
    }

    Loader {
        // NOTE: re-enable when adding a provider that requires an API key
        active: false;// mainPageStack.depth === 1 && !OpenWeatherMap.key
        anchors {
            fill: parent
        }
        asynchronous: true
        source: "../components/NoAPIKeyErrorStateComponent.qml"
        visible: status === Loader.Ready && active
    }
}
