/*
 * Copyright (C) 2015, 2017 Canonical Ltd.
 *
 * This file is part of Lomiri Weather App
 *
 * Lomiri Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Lomiri Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import "../components"
import "../components/HeadState"
import "../components/ListItemActions"


Page {
    id: locationsListPage
    objectName: "locationsListPage"
    state: locationsListView.state === "multiselectable" ? "selection" : "default"
    states: [
        LocationsHeadState {
            thisPage: locationsListPage
        },
        MultiSelectHeadState {
            listview: locationsListView
            removable: true
            thisPage: locationsListPage

            onRemoved: storage.removeMultiLocations(selectedIndices.slice());
        }
    ]

    // This is used by the header state (LocationsHeadState) to go back
    signal pop()

    ListModel {
        id: currentLocationModel
    }

    MultiSelectListView {
        id: locationsListView
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            top: locationsListPage.header.bottom
        }
        model: ListModel {
            id: locationsModel
        }
        header: MultiSelectListView {
            id: currentLocationListView
            anchors {
                left: parent.left
                right: parent.right
            }
            height: settings.addedCurrentLocation && settings.detectCurrentLocation ? contentHeight : units.gu(0)
            interactive: false
            model: currentLocationModel
            delegate: WeatherListItem {
                id: currentLocationListItem
                height: currentListItemLayout.height + (divider.visible ? divider.height : 0) + (fakeDivider.visible ? fakeDivider.height : 0)

                onItemClicked: {
                    settings.current = index;

                    // Ensure any selections are closed
                    locationsListView.closeSelection();

                    locationsListPage.pop()
                }

                ListItemLayout {
                    id: currentListItemLayout
                    // uncomment to show locations index in front of location name
                    // Label {
                    //     anchors {
                    //         verticalCenter: parent.verticalCenter
                    //     }
                    //     font {
                    //         pixelSize: parent.height / 3
                    //         weight: Font.Light
                    //     }
                    //     horizontalAlignment: Text.AlignLeft
                    //     SlotsLayout.position: SlotsLayout.Leading
                    //     SlotsLayout.overrideVerticalPositioning: true
                    //     text: index+1
                    // }
                    title {
                        elide: Text.ElideRight
                        fontSize: "medium"
                        text: i18n.tr("Current Location")
                    }
                    subtitle {
                        elide: Text.ElideRight
                        fontSize: "small"
                        text: name + ", " + (adminName1 == name ? countryName : adminName1)
                    }
                    summary {
                        elide: Text.ElideRight
                        fontSize: "small"
                        text: provider_coords
                    }

                    Icon {
                        anchors {
                            verticalCenter: parent.verticalCenter
                        }
                        height: parent.height / 2
                        width: height
                        name: icon
                        SlotsLayout.position: SlotsLayout.Trailing
                        SlotsLayout.overrideVerticalPositioning: true

                    }

                    Label {
                        anchors {
                            verticalCenter: parent.verticalCenter
                        }
                        width: units.gu(8.5)  //needed to align weather icon regardless of number of digits
                        font {
                            pixelSize: parent.height / 3
                            weight: Font.Light
                        }
                        horizontalAlignment: Text.AlignRight
                        SlotsLayout.position: SlotsLayout.Trailing
                        SlotsLayout.overrideVerticalPositioning: true
                        text: temp
                    }
                }

                // Inject extra divider when we are the last as the SDK hides it
                // but we need one as we have another listview directly below
                ListItem {
                    id: fakeDivider
                    anchors {
                        bottom: parent.bottom
                    }
                    divider {
                        visible: true
                    }
                    height: divider.height
                    visible: currentLocationModel.count - 1 == index
                }
            }
        }

        delegate: WeatherListItem {
            id: locationsListItem
            height: listItemLayout.height + (divider.visible ? divider.height : 0)
            leadingActions: ListItemActions {
                actions: [
                    Remove {
                        onTriggered: storage.removeLocation(index)
                    }
                ]
            }
            multiselectable: true
            objectName: "location" + index
            reorderable: true

            onItemClicked: {
                if (settings.addedCurrentLocation && settings.detectCurrentLocation) {
                    settings.current = index + 1;
                } else {
                    settings.current = index;
                }

                locationsListPage.pop()
            }

            ListItemLayout {
                id: listItemLayout
                // uncomment to show locations index infront of location name
                // Label {
                //     anchors.verticalCenter: parent.verticalCenter
                //     font {
                //         pixelSize: parent.height / 4
                //         weight: Font.Light
                //     }
                //     horizontalAlignment: Text.AlignLeft
                //     SlotsLayout.position: SlotsLayout.Leading
                //     SlotsLayout.overrideVerticalPositioning: true
                //     text: index+1
                // }
                title {
                    elide: Text.ElideRight
                    fontSize: "medium"
                    text: name
                }
                subtitle {
                    elide: Text.ElideRight
                    fontSize: "small"
                    text: adminName1 == name ? countryName :  adminName1 + ", " + countryName
                }
                summary {
                    elide: Text.ElideRight
                    fontSize: "small"
                    text: geonames_id + "\n" + geonames_coords
                }

                Icon {
                    anchors {
                        verticalCenter: parent.verticalCenter
                    }
                    height: parent.height / 2
                    width: height
                    name: icon
                    SlotsLayout.position: SlotsLayout.Trailing
                    SlotsLayout.overrideVerticalPositioning: true
                    visible: locationsListView.state === "default"
                }

                Label {
                    anchors {
                        verticalCenter: parent.verticalCenter
                    }
                    width: units.gu(8.5)  //needed to align weather icon regardless of number of digits
                    font {
                        pixelSize: parent.height / 3
                        weight: Font.Light
                    }
                    horizontalAlignment: Text.AlignRight
                    SlotsLayout.position: SlotsLayout.Trailing
                    SlotsLayout.overrideVerticalPositioning: true
                    text: temp
                    visible: locationsListView.state === "default"
                }
            }
        }

        onReorder: {
            console.debug("Move: ", from, to);

            storage.moveLocation(from, to);
        }
    }

    LoadingIndicator {
        id: loadingIndicator
    }

    Loader {
        id: pageLoader
        active: locationsList === null || locationsList.length === 0
        anchors {
            fill: parent
        }
        asynchronous: true
        source: "../components/LocationsListPageEmptyStateComponent.qml"
        visible: status === Loader.Ready && active
    }

    function populateLocationsModel() {
        currentLocationModel.clear()
        locationsModel.clear()
        var loc = {}, data = {};

        for (var i=0; i < weatherApp.locationsList.length; i++) {
            data = weatherApp.locationsList[i];
            loc = {
                "name": data.location.name,
                "adminName1": data.location.adminName1,
                "areaLabel": data.location.areaLabel,
                "countryName": data.location.countryName,
                "temp": getTemp(data.data[0].current["metric"].temp,true),
                "icon": iconMap[data.data[0].current.icon],
                // "provider_id": i18n.ctr("Location ID from weather-data provider","Station ID: %1").arg(data.data[0].current.service_id),
                // provider coords are quite a bit off with open-meteo.com data, so for the moment we better use geonames.org station data
                // "provider_coords": i18n.ctr("Latitude and longitude (coordinates)","lat: %1, lon: %2").arg(data.data[0].current.coordLAT).arg(data.data[0].current.coordLON),
                // use the function localeDezimalSeparator() to convert the "11.222, 33.444" formating to use the locale-decimal separator if needed, then use ; to separate the two numbers
                "geonames_id": i18n.ctr("Location ID from geonames.org","geonames.org ID: %1").arg(data.location.services.geonames),
                "geonames_coords": i18n.ctr("Latitude and longitude (coordinates)","lat: %1, lon: %2").arg(data.location.coord.lat).arg(data.location.coord.lon)
            }

            if (!settings.addedCurrentLocation || i > 0) {
                locationsModel.append(loc)
            } else {
                currentLocationModel.append(loc)
            }
        }
    }

    Connections {
        target: weatherApp
        onLocationsListChanged: populateLocationsModel()
    }

    Component.onCompleted: populateLocationsModel()
}
