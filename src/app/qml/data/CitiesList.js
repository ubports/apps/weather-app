.pragma library
/*
 * Copyright (C) 2020 UBports Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

//cities with more than 5 million population, based on data from http://download.geonames.org/export/dump/cities15000.zip, downloaded 2020-04-19

 var megacities = [
   {"lon":"44.40088","lat":"33.34058"},  //Baghdad, pop: 7216000, last mod date: 2016-06-22
   {"lon":"100.50144","lat":"13.75398"},  //Bangkok, pop: 5104476, last mod date: 2017-03-29
   {"lon":"116.39723","lat":"39.9075"},  //Beijing, pop: 11716620, last mod date: 2019-09-05
   {"lon":"77.59369","lat":"12.97194"},  //Bengaluru, pop: 5104047, last mod date: 2019-09-05
   {"lon":"-74.08175","lat":"4.60971"},  //Bogota, pop: 7674366, last mod date: 2019-10-08
   {"lon":"-58.37723","lat":"-34.61315"},  //Buenos Aires, pop: 13076300, last mod date: 2019-09-05
   {"lon":"31.24967","lat":"30.06263"},  //Cairo, pop: 7734614, last mod date: 2015-06-03
   {"lon":"104.06667","lat":"30.66667"},  //Chengdu, pop: 7415590, last mod date: 2019-02-26
   {"lon":"106.55278","lat":"29.56278"},  //Chongqing, pop: 7457600, last mod date: 2019-09-19
   {"lon":"77.23149","lat":"28.65195"},  //Delhi, pop: 10927986, last mod date: 2019-09-05
   {"lon":"90.40744","lat":"23.7104"},  //Dhaka, pop: 10356500, last mod date: 2020-03-18
   {"lon":"113.74866","lat":"23.01797"},  //Dongguan, pop: 8000000, last mod date: 2019-02-26
   {"lon":"113.25","lat":"23.11667"},  //Guangzhou, pop: 11071424, last mod date: 2017-06-20
   {"lon":"120.16142","lat":"30.29365"},  //Hangzhou, pop: 6241971, last mod date: 2019-09-05
   {"lon":"126.65","lat":"45.75"},  //Harbin, pop: 5878939, last mod date: 2019-09-05
   {"lon":"114.17469","lat":"22.27832"},  //Hong Kong, pop: 7012738, last mod date: 2017-05-23
   {"lon":"28.94966","lat":"41.01384"},  //Istanbul, pop: 14804116, last mod date: 2017-09-26
   {"lon":"106.84513","lat":"-6.21462"},  //Jakarta, pop: 8540121, last mod date: 2019-09-05
   {"lon":"67.0104","lat":"24.8608"},  //Karachi, pop: 11624219, last mod date: 2019-12-30
   {"lon":"15.31357","lat":"-4.32758"},  //Kinshasa, pop: 7785965, last mod date: 2019-09-05
   {"lon":"3.39467","lat":"6.45407"},  //Lagos, pop: 9000000, last mod date: 2019-09-05
   {"lon":"74.35071","lat":"31.558"},  //Lahore, pop: 6310888, last mod date: 2019-12-06
   {"lon":"-77.02824","lat":"-12.04318"},  //Lima, pop: 7737002, last mod date: 2019-09-05
   {"lon":"-0.12574","lat":"51.50853"},  //London, pop: 7556900, last mod date: 2019-09-18
   {"lon":"-99.12766","lat":"19.42847"},  //Mexico City, pop: 12294193, last mod date: 2019-03-15
   {"lon":"37.61556","lat":"55.75222"},  //Moscow, pop: 10381222, last mod date: 2020-03-31
   {"lon":"72.88261","lat":"19.07283"},  //Mumbai, pop: 12691836, last mod date: 2019-09-05
   {"lon":"106.08473","lat":"30.79508"},  //Nanchong, pop: 7150000, last mod date: 2012-01-18
   {"lon":"118.77778","lat":"32.06167"},  //Nanjing, pop: 7165292, last mod date: 2019-09-05
   {"lon":"-74.00597","lat":"40.71427"},  //New York City, pop: 8175133, last mod date: 2019-09-23
   {"lon":"-43.18223","lat":"-22.90642"},  //Rio de Janeiro, pop: 6023699, last mod date: 2019-09-05
   {"lon":"30.31413","lat":"59.93863"},  //Saint Petersburg, pop: 5028000, last mod date: 2020-01-16
   {"lon":"-46.63611","lat":"-23.5475"},  //Sao Paulo, pop: 10021295, last mod date: 2019-07-12
   {"lon":"126.9784","lat":"37.566"},  //Seoul, pop: 10349312, last mod date: 2019-09-05
   {"lon":"121.45806","lat":"31.22222"},  //Shanghai, pop: 22315474, last mod date: 2017-07-27
   {"lon":"116.71479","lat":"23.36814"},  //Shantou, pop: 5329024, last mod date: 2018-07-23
   {"lon":"123.43278","lat":"41.79222"},  //Shenyang, pop: 6255921, last mod date: 2019-09-05
   {"lon":"114.0683","lat":"22.54554"},  //Shenzhen, pop: 10358381, last mod date: 2019-09-05
   {"lon":"120.59538","lat":"31.30408"},  //Suzhou, pop: 5345961, last mod date: 2018-12-14
   {"lon":"117.12","lat":"36.18528"},  //Tai'an, pop: 5499000, last mod date: 2012-01-18
   {"lon":"121.53185","lat":"25.04776"},  //Taipei, pop: 7871900, last mod date: 2017-09-26
   {"lon":"51.42151","lat":"35.69439"},  //Tehran, pop: 7153309, last mod date: 2015-10-03
   {"lon":"117.17667","lat":"39.14222"},  //Tianjin, pop: 11090314, last mod date: 2017-06-29
   {"lon":"139.69171","lat":"35.6895"},  //Tokyo, pop: 8336599, last mod date: 2019-07-09
   {"lon":"114.26667","lat":"30.58333"},  //Wuhan, pop: 9785388, last mod date: 2019-09-05
   {"lon":"108.92861","lat":"34.25833"},  //Xi'an, pop: 6501190, last mod date: 2019-02-26
]
