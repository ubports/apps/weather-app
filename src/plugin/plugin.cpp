/*
 * Copyright (C) 2024 Maciej Sopyło <me@klh.io>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This file is part of Lomiri Weather App
 *
 * Lomiri Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Lomiri Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QtQml>
#include <QtQml/QQmlContext>

#include "plugin.h"
#include "provider.h"
#include "weather_data_provider.h"
#include "open_meteo/open_meteo_weather_provider.h"

void WeatherPlugin::registerTypes(const char *uri)
{
    qmlRegisterUncreatableType<DataPoint>(uri, 1, 0, "DataPoint", "Do not create from QML");
    qmlRegisterInterface<WeatherDataProviderInterface>("WeatherDataProviderInterface");

    qmlRegisterSingletonType<Provider>(
            uri, 1, 0, "Provider",
            [](QQmlEngine *, QJSEngine *) -> QObject * { return new Provider; });
    qmlRegisterSingletonType<OpenMeteoWeatherProvider>(
            uri, 1, 0, "OpenMeteoWeatherProvider",
            [](QQmlEngine *, QJSEngine *) -> QObject * { return new OpenMeteoWeatherProvider; });
}
