/*
 * Copyright (C) 2024 Maciej Sopyło <me@klh.io>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This file is part of Lomiri Weather App
 *
 * Lomiri Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Lomiri Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DATA_POINT_H
#define DATA_POINT_H

#include <QObject>
#include <QVariant>
#include <QDateTime>
#include <QDate>
#include <QGeoCoordinate>
#include <QScopedPointer>

class DataPoint
{
    Q_GADGET
    Q_PROPERTY(QDateTime timestamp MEMBER m_timestamp CONSTANT)
    Q_PROPERTY(QVariant temperature MEMBER m_temperature CONSTANT)
    Q_PROPERTY(QVariant temperatureMin MEMBER m_temperatureMin CONSTANT)
    Q_PROPERTY(QVariant temperatureMax MEMBER m_temperatureMax CONSTANT)
    Q_PROPERTY(QVariant feelsLike MEMBER m_feelsLike CONSTANT)
    Q_PROPERTY(QVariant feelsLikeMin MEMBER m_feelsLikeMin CONSTANT)
    Q_PROPERTY(QVariant feelsLikeMax MEMBER m_feelsLikeMax CONSTANT)
    Q_PROPERTY(QVariant humidity MEMBER m_humidity CONSTANT)
    Q_PROPERTY(QVariant pressure MEMBER m_pressure CONSTANT)
    Q_PROPERTY(QVariant visibility MEMBER m_visibility CONSTANT)
    Q_PROPERTY(QVariant sunshineDuration MEMBER m_sunshineDuration CONSTANT)
    Q_PROPERTY(QVariant uvIndex MEMBER m_uvIndex CONSTANT)
    Q_PROPERTY(QVariant uvIndexDay MEMBER m_uvIndexDay CONSTANT)
    Q_PROPERTY(QVariant windSpeed MEMBER m_windSpeed CONSTANT)
    Q_PROPERTY(QVariant windGusts MEMBER m_windGusts CONSTANT)
    Q_PROPERTY(QVariant windAzimuth MEMBER m_windAzimuth CONSTANT)
    Q_PROPERTY(QVariant windDirection MEMBER m_windDirection CONSTANT)
    Q_PROPERTY(QVariant cloudCover MEMBER m_cloudCover CONSTANT)
    Q_PROPERTY(QVariant precipitation MEMBER m_precipitation CONSTANT)
    Q_PROPERTY(QVariant snowfall MEMBER m_snowfall CONSTANT)
    Q_PROPERTY(QVariant precipitationProbability MEMBER m_precipitationProbability CONSTANT)
    Q_PROPERTY(QVariant rain MEMBER m_rain CONSTANT)
    Q_PROPERTY(QVariant showers MEMBER m_showers CONSTANT)
    Q_PROPERTY(QVariant snowDepth MEMBER m_snowDepth CONSTANT)
    Q_PROPERTY(WeatherCode weatherCode MEMBER m_weatherCode CONSTANT)
    Q_PROPERTY(QVariant isDay MEMBER m_isDay CONSTANT)
    Q_PROPERTY(QString description READ description CONSTANT)
    Q_PROPERTY(QString icon READ icon CONSTANT)
    Q_PROPERTY(QString windDirectionString READ windDirectionString CONSTANT)

public:
    DataPoint(){};

    // https://www.nodc.noaa.gov/archive/arc0021/0002199/1.1/data/0-data/HTML/WMO-CODE/WMO4677.HTM
    enum WeatherCode {
        Clear = 0,
        DissolvingClouds = 1,
        PartlyCloudy = 2,
        Overcast = 3,
        SmokeAsh = 4,
        Haze = 5,
        LightDust = 6,
        Dust = 7,
        HeavyDust = 8,
        DustStormClose = 9,
        Mist = 10,
        LightFog = 11,
        ContinuousFog = 12,
        LightningFar = 13,
        Precipitation = 14,
        PrecipitationDistant = 15,
        PrecipitationNear = 16,
        ThunderstormDry = 17,
        Squalls = 18,
        FunnelClouds = 19,

        // Precipitation, fog, ice fog or thunderstorm (during the preceding hour, not at the time)
        Drizzle = 20,
        Rain = 21,
        Snow = 22,
        RainAndSnow = 23,
        FreezingRain = 24,
        RainShower = 25,
        SnowShower = 26,
        HailShower = 27,
        Fog = 28,
        Thunderstorm = 29,

        // Duststorm, sandstorm, drifting or blowing snow
        DustStormDecreasing = 30,
        DustStorm = 31,
        DustStormIncreasing = 32,
        SevereDustStormDecreasing = 33,
        SevereDustStorm = 34,
        SevereDustStormIncreasing = 35,
        // low - below eye level, high - above eye level
        BlowingSnowLow = 36,
        HeavyDriftingSnowLow = 37,
        BlowingSnowHigh = 38,
        HeavyDriftingSnowHigh = 39,

        // Fog or ice fog (at the time of observation)
        FogAtDistance = 40,
        FogPatches = 41,
        FogSkyVisibleDecreasing = 42,
        FogSkyInvisibleDecreasing = 43,
        FogSkyVisible = 44,
        FogSkyInvisible = 45,
        FogSkyVisibleIncreasing = 46,
        FogSkyInvisibleIncreasing = 47,
        FogRimeSkyVisible = 48,
        FogRimeSkyInvisible = 49,

        // Precipitation at the time of observation
        DrizzleIntermittentSlight = 50,
        DrizzleContinuousSlight = 51,
        DrizzleIntermittent = 52,
        DrizzleContinuous = 53,
        DrizzleIntermittentHeavy = 54,
        DrizzleContinuousHeavy = 55,
        DrizzleFreezingSlight = 56,
        DrizzleFreezingHeavy = 57,
        DrizzleRainSlight = 58,
        DrizzleRainHeavy = 59,

        // Rain
        RainIntermittentSlight = 60,
        RainContinuousSlight = 61,
        RainIntermittent = 62,
        RainContinuous = 63,
        RainIntermittentHevy = 64,
        RainContinuousHeavy = 65,
        RainFreezingSlight = 66,
        RainFreezingHeavy = 67,
        RainDrizzleSnowSlight = 68,
        RainDrizzleSnowHeavy = 69,

        // Solid precipitation not in showers
        SnowflakesIntermittentSlight = 70,
        SnowflakesContinuousSlight = 71,
        SnowflakesIntermittent = 72,
        SnowflakesContinuous = 73,
        SnowflakesIntermittentHeavy = 74,
        SnowflakesContinuousHeavy = 75,
        DiamondDust = 76,
        SnowGrains = 77,
        SnowStars = 78,
        IcePellets = 79,

        // Shower precipitation or with thunderstorms
        RainShowerSlight = 80,
        RainShowerHeavy = 81,
        RainShowerViolent = 82,
        RainSnowShowerSlight = 83,
        RainSnowShowerHeavy = 84,
        SnowShowerSlight = 85,
        SnowShowerHeavy = 86,
        SmallHailSlight = 87,
        SmallHailHeavy = 88,
        HailNoThunderSlight = 89,
        HailNoThunderHeavy = 90,
        RainSlightAfterThunderstorm = 91,
        RainHeavyAfterThunderstorm = 92,
        SnowOrHailSlightAfterThunderstorm = 93,
        SnowOrHailHeavyAfterThunderstorm = 94,
        ThunderstormWithoutHailSlight = 95,
        ThunderstormWithHailSlight = 96,
        ThunderstormWithoutHailHeavy = 97,
        ThunderstormWithDuststorm = 98,
        ThunderstormWithHailHeavy = 99,
    };
    Q_ENUM(WeatherCode)

    enum Direction {
        North,
        NorthEast,
        East,
        SouthEast,
        South,
        SouthWest,
        West,
        NorthWest,
    };
    Q_ENUM(Direction)

    DataPoint(QDateTime timestamp);

    void setTemperature(qreal value) { m_temperature.setValue(value); }
    void setTemperatureMin(qreal value) { m_temperatureMin.setValue(value); }
    void setTemperatureMax(qreal value) { m_temperatureMax.setValue(value); }
    void setWeatherCode(WeatherCode value) { m_weatherCode = value; }
    void setFeelsLike(qreal value) { m_feelsLike.setValue(value); }
    void setFeelsLikeMin(qreal value) { m_feelsLikeMin.setValue(value); }
    void setFeelsLikeMax(qreal value) { m_feelsLikeMax.setValue(value); }
    void setHumidity(qreal value) { m_humidity.setValue(value); }
    void setPressure(qreal value) { m_pressure.setValue(value); }
    void setUvIndex(qreal value) { m_uvIndex.setValue(value); }
    void setUvIndexDay(qreal value) { m_uvIndexDay.setValue(value); }
    void setVisibility(qreal value) { m_visibility.setValue(value); }
    void setSunshineDuration(qreal value) { m_sunshineDuration.setValue(value); }
    void setWindSpeed(qreal value) { m_windSpeed.setValue(value); }
    void setWindAzimuth(qreal value) { m_windAzimuth.setValue(value); }
    void setWindDirection(qreal value) { m_windDirection.setValue(value); }
    void setWindGusts(qreal value) { m_windGusts.setValue(value); }
    void setCloudCover(qreal value) { m_cloudCover.setValue(value); }
    void setPrecipitation(qreal value) { m_precipitation.setValue(value); }
    void setSnowfall(qreal value) { m_snowfall.setValue(value); }
    void setPrecipitationProbability(qreal value) { m_precipitationProbability.setValue(value); }
    void setRain(qreal value) { m_rain.setValue(value); }
    void setShowers(qreal value) { m_showers.setValue(value); }
    void setSnowDepth(qreal value) { m_snowDepth.setValue(value); }
    void setIsDay(bool value) { m_isDay.setValue(value); }

    QString description() const;
    QString icon() const;
    QString windDirectionString() const;

private:
    QDateTime m_timestamp;
    QVariant m_temperature;
    QVariant m_temperatureMin;
    QVariant m_temperatureMax;
    QVariant m_feelsLike;
    QVariant m_feelsLikeMin;
    QVariant m_feelsLikeMax;
    QVariant m_humidity;
    QVariant m_pressure;
    QVariant m_uvIndex;
    QVariant m_uvIndexDay;
    QVariant m_visibility;
    QVariant m_sunshineDuration;
    QVariant m_windSpeed;
    QVariant m_windAzimuth;
    QVariant m_windDirection;
    QVariant m_windGusts;
    QVariant m_cloudCover;
    QVariant m_precipitation;
    QVariant m_snowfall;
    QVariant m_precipitationProbability;
    QVariant m_rain;
    QVariant m_showers;
    QVariant m_snowDepth;
    WeatherCode m_weatherCode;
    QVariant m_isDay;
};

Q_DECLARE_METATYPE(DataPoint)

#endif // DATA_POINT_H
