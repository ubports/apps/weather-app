/*
 * Copyright (C) 2024 Maciej Sopyło <me@klh.io>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This file is part of Lomiri Weather App
 *
 * Lomiri Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Lomiri Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "data_point.h"

#include <libintl.h>

DataPoint::DataPoint(QDateTime timestamp)
{
    m_timestamp = timestamp;
}

QString DataPoint::description() const
{
    switch (m_weatherCode) {
    case Clear:
        return gettext("clear");
    case DissolvingClouds:
        return gettext("dissolving clouds");
    case PartlyCloudy:
        return gettext("partly cloudly");
    case Overcast:
        return gettext("overcast");
    case SmokeAsh:
        return gettext("smoke or ash");
    case Haze:
        return gettext("haze");
    case LightDust:
        return gettext("light dust");
    case Dust:
        return gettext("dust");
    case HeavyDust:
        return gettext("heavy dust");
    case DustStormClose:
        return gettext("nearby dust storm");
    case Mist:
        return gettext("mist");
    case LightFog:
        return gettext("light fog");
    case ContinuousFog:
        return gettext("continuous fog");
    case LightningFar:
        return gettext("visible lightning");
    case Precipitation:
        return gettext("precipitation");
    case PrecipitationDistant:
        return gettext("distant precipitation");
    case PrecipitationNear:
        return gettext("nearby precipitation");
    case ThunderstormDry:
        return gettext("dry thunderstorm");
    case Squalls:
        return gettext("squalls");
    case FunnelClouds:
        return gettext("funnel clouds");
    case Drizzle:
        return gettext("drizzle");
    case Rain:
        return gettext("rain");
    case Snow:
        return gettext("snow");
    case RainAndSnow:
        return gettext("rain and snow");
    case FreezingRain:
        return gettext("freezing rain");
    case RainShower:
        return gettext("rain shower");
    case SnowShower:
        return gettext("snow shower");
    case HailShower:
        return gettext("hail shower");
    case Fog:
        return gettext("fog");
    case Thunderstorm:
        return gettext("thunderstorm");
    case DustStormDecreasing:
        return gettext("decreasing dust storm");
    case DustStorm:
        return gettext("dust storm");
    case DustStormIncreasing:
        return gettext("increasing dust storm");
    case SevereDustStormDecreasing:
        return gettext("decreasing severe dust storm");
    case SevereDustStorm:
        return gettext("severe dust storm");
    case SevereDustStormIncreasing:
        return gettext("increasing severe dust storm");
    case BlowingSnowLow:
        return gettext("low blowing snow");
    case HeavyDriftingSnowLow:
        return gettext("low heavy drifting snow");
    case BlowingSnowHigh:
        return gettext("high blowing snow");
    case HeavyDriftingSnowHigh:
        return gettext("high heavy drifting snow");
    case FogAtDistance:
        return gettext("distant fog");
    case FogPatches:
        return gettext("patchy fog");
    case FogSkyVisibleDecreasing:
    case FogSkyInvisibleDecreasing:
        return gettext("decreasing fog");
    case FogSkyVisible:
    case FogSkyInvisible:
        return gettext("fog");
    case FogSkyVisibleIncreasing:
    case FogSkyInvisibleIncreasing:
        return gettext("increasing fog");
    case FogRimeSkyVisible:
    case FogRimeSkyInvisible:
        return gettext("fog with rime");
    case DrizzleIntermittentSlight:
        return gettext("intermittent slight drizzle");
    case DrizzleContinuousSlight:
        return gettext("continuous slight drizzle");
    case DrizzleIntermittent:
        return gettext("intermittent drizzle");
    case DrizzleContinuous:
        return gettext("continuous drizzle");
    case DrizzleIntermittentHeavy:
        return gettext("intermittent heavy drizzle");
    case DrizzleContinuousHeavy:
        return gettext("continuous heavy drizzle");
    case DrizzleFreezingSlight:
        return gettext("freezing slight drizzle");
    case DrizzleFreezingHeavy:
        return gettext("freezing heavy drizzle");
    case DrizzleRainSlight:
        return gettext("slight rain drizzle");
    case DrizzleRainHeavy:
        return gettext("heavy rain drizzle");
    case RainIntermittentSlight:
        return gettext("intermittent slight rain");
    case RainContinuousSlight:
        return gettext("continuous slight rain");
    case RainIntermittent:
        return gettext("intermittent rain");
    case RainContinuous:
        return gettext("continuous rain");
    case RainIntermittentHevy:
        return gettext("intermittent heavy rain");
    case RainContinuousHeavy:
        return gettext("continuous heavy rain");
    case RainFreezingSlight:
        return gettext("freezing slight rain");
    case RainFreezingHeavy:
        return gettext("freezing heavy rain");
    case RainDrizzleSnowSlight:
        return gettext("slight rain drizzle with snow");
    case RainDrizzleSnowHeavy:
        return gettext("heavy rain drizzle with snow");
    case SnowflakesIntermittentSlight:
        return gettext("intermittent slight snowflakes");
    case SnowflakesContinuousSlight:
        return gettext("continuous slight snowflakes");
    case SnowflakesIntermittent:
        return gettext("intermittent snowflakes");
    case SnowflakesContinuous:
        return gettext("continuous snowflakes");
    case SnowflakesIntermittentHeavy:
        return gettext("intermittent heavy snowflakes");
    case SnowflakesContinuousHeavy:
        return gettext("continuous heavy snowflakes");
    case DiamondDust:
        return gettext("diamond dust");
    case SnowGrains:
        return gettext("snow grains");
    case SnowStars:
        return gettext("snow stars");
    case IcePellets:
        return gettext("ice pellets");
    case RainShowerSlight:
        return gettext("slight rain shower");
    case RainShowerHeavy:
        return gettext("heavy rain shower");
    case RainShowerViolent:
        return gettext("violent rain shower");
    case RainSnowShowerSlight:
        return gettext("slight rain shower with snow");
    case RainSnowShowerHeavy:
        return gettext("heavy rain shower with snow");
    case SnowShowerSlight:
        return gettext("slight snow shower");
    case SnowShowerHeavy:
        return gettext("heavy snow shower");
    case SmallHailSlight:
        return gettext("slight small hail");
    case SmallHailHeavy:
        return gettext("heavy small hail");
    case HailNoThunderSlight:
        return gettext("slight hail without thunder");
    case HailNoThunderHeavy:
        return gettext("heavy hail without thunder");
    case RainSlightAfterThunderstorm:
        return gettext("slight rain after thunderstorm");
    case RainHeavyAfterThunderstorm:
        return gettext("heavy rain after thunderstorm");
    case SnowOrHailSlightAfterThunderstorm:
        return gettext("slight snow or hail after thunderstorm");
    case SnowOrHailHeavyAfterThunderstorm:
        return gettext("heavy snow or hail after thunderstorm");
    case ThunderstormWithoutHailSlight:
        return gettext("slight thunderstorm without hail");
    case ThunderstormWithHailSlight:
        return gettext("slight thunderstorm with hail");
    case ThunderstormWithoutHailHeavy:
        return gettext("heavy thunderstorm without hail");
    case ThunderstormWithDuststorm:
        return gettext("thunderstorm with dust storm");
    case ThunderstormWithHailHeavy:
        return gettext("heavy thunderstorm with hail");
    }

    return "";
}

// TODO: we are missing A LOT of icons
QString DataPoint::icon() const
{
    bool isNight = !m_isDay.isNull() && !m_isDay.toBool();
    switch (m_weatherCode) {
    case Clear:
        return isNight ? "moon" : "sun";
    case DissolvingClouds:
        return QString("few_cloud_%1").arg(isNight ? "moon" : "sun");
    case PartlyCloudy:
        return "scattered_cloud";
    case Overcast:
        return "cloud";
    case SmokeAsh:
    case Haze:
    case LightDust:
    case Dust:
    case HeavyDust:
    case DustStormClose:
        return "snow";
    case Mist:
    case LightFog:
    case ContinuousFog:
        return "fog";
    case LightningFar:
        return "thunder";
    case Precipitation:
    case PrecipitationDistant:
    case PrecipitationNear:
        return "rain";
    case ThunderstormDry:
        return "thunder";
    case Squalls:
        return "cloud"; // wind would fit better
    case FunnelClouds:
        return "scattered_cloud";
    case Drizzle:
    case Rain:
        return "rain";
    case Snow:
    case RainAndSnow:
        return "snow";
    case FreezingRain:
        return "rain";
    case RainShower:
    case SnowShower:
    case HailShower:
        return "shower";
    case Fog:
        return "fog";
    case Thunderstorm:
        return "thunder";
    case DustStormDecreasing:
    case DustStorm:
    case DustStormIncreasing:
    case SevereDustStormDecreasing:
    case SevereDustStorm:
    case SevereDustStormIncreasing:
    // low - below eye level, high - above eye level
    case BlowingSnowLow:
    case HeavyDriftingSnowLow:
    case BlowingSnowHigh:
    case HeavyDriftingSnowHigh:
        return "snow";
    case FogAtDistance:
    case FogPatches:
    case FogSkyVisibleDecreasing:
    case FogSkyInvisibleDecreasing:
    case FogSkyVisible:
    case FogSkyInvisible:
    case FogSkyVisibleIncreasing:
    case FogSkyInvisibleIncreasing:
    case FogRimeSkyVisible:
    case FogRimeSkyInvisible:
        return "fog";
    case DrizzleIntermittentSlight:
    case DrizzleContinuousSlight:
    case DrizzleIntermittent:
    case DrizzleContinuous:
    case DrizzleIntermittentHeavy:
    case DrizzleContinuousHeavy:
    case DrizzleFreezingSlight:
    case DrizzleFreezingHeavy:
    case DrizzleRainSlight:
    case DrizzleRainHeavy:
    case RainIntermittentSlight:
    case RainContinuousSlight:
    case RainIntermittent:
    case RainContinuous:
    case RainIntermittentHevy:
    case RainContinuousHeavy:
    case RainFreezingSlight:
    case RainFreezingHeavy:
    case RainDrizzleSnowSlight:
    case RainDrizzleSnowHeavy:
        return "rain";
    case SnowflakesIntermittentSlight:
    case SnowflakesContinuousSlight:
    case SnowflakesIntermittent:
    case SnowflakesContinuous:
    case SnowflakesIntermittentHeavy:
    case SnowflakesContinuousHeavy:
    case DiamondDust:
    case SnowGrains:
    case SnowStars:
    case IcePellets:
        return "snow";
    case RainShowerSlight:
    case RainShowerHeavy:
    case RainShowerViolent:
    case RainSnowShowerSlight:
    case RainSnowShowerHeavy:
        return "showers";
    case SnowShowerSlight:
    case SnowShowerHeavy:
    case SmallHailSlight:
    case SmallHailHeavy:
    case HailNoThunderSlight:
    case HailNoThunderHeavy:
        return "snow"; // hail, not snow
    case RainSlightAfterThunderstorm:
    case RainHeavyAfterThunderstorm:
        return "rain";
    case SnowOrHailSlightAfterThunderstorm:
    case SnowOrHailHeavyAfterThunderstorm:
        return "snow";
    case ThunderstormWithoutHailSlight:
    case ThunderstormWithHailSlight:
    case ThunderstormWithoutHailHeavy:
    case ThunderstormWithDuststorm:
    case ThunderstormWithHailHeavy:
        return "thunder";
    }

    return "";
}

QString DataPoint::windDirectionString() const
{
    if (m_windDirection.isNull() || !m_windDirection.canConvert<Direction>()) {
        return "";
    }

    switch (m_windDirection.value<Direction>()) {

    case North:
        return "N";
    case NorthEast:
        return "NE";
    case East:
        return "E";
    case SouthEast:
        return "SE";
    case South:
        return "S";
    case SouthWest:
        return "SW";
    case West:
        return "W";
    case NorthWest:
        return "NW";
    }

    return "";
}
